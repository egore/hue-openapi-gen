package de.egore911.hue.openapi;

import java.util.ArrayList;
import java.util.List;

public class ServiceC {
    public String uri;
    public List<ResponseC> responses = new ArrayList<>();
    public String method;
    public RequestC request;
    public TagC tag;
}
