package de.egore911.hue.openapi;

import java.util.ArrayList;
import java.util.List;

public class TypeC {
    public boolean array;
    public String name;
    public List<PropertyC> properties = new ArrayList<>();
    public boolean builtin;
    public String pattern;
    public boolean enum_;
    public Integer maximum;
    public Integer minimum;
    public Integer minLength;
    public Integer maxLength;
    public Integer minItems;
    public Integer maxItems;
    public String default_;
}
