package de.egore911.hue;

import de.egore911.hue.openapi.PropertyC;
import de.egore911.hue.openapi.RequestC;
import de.egore911.hue.openapi.ResponseC;
import de.egore911.hue.openapi.ServiceC;
import de.egore911.hue.openapi.TagC;
import de.egore911.hue.openapi.TypeC;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class HTMLParser {

    List<TypeC> types = new ArrayList<>();
    List<TagC> tags = new ArrayList<>();
    List<ServiceC> services = new ArrayList<>();

    private final File inputFile;

    public HTMLParser(String filename) {
        var path = Path.of(filename);
        if (!Files.exists(path)) {
            throw new UnsupportedOperationException("Cannot work with non-existing files");
        }
        this.inputFile = path.toFile();
    }

    public void parse() throws IOException {
        var doc = Jsoup.parse(inputFile);

        // Search for the GET, PUT, POST, etc. badges, which provide the Javascript links for popup windows
        Elements select = doc.select("span.methods > a > span.badge");
        for (var badge : select) {

            // Get the parent which must be the popup link
            Element popupLink = badge.parent();
            assert popupLink != null;
            if (!popupLink.tagName().equals("a")) {
                Logger.getLogger(HTMLParser.class.getName()).warning("The parent of the badge found was not an <a> tag, skipping the badge");
                continue;
            }

            // The href is the ID of the popup window, which has two children (one for the request, on for the response)
            String id = popupLink.attr("href");

            // Build a new service
            // TODO introduce "method" layer
            var service = new ServiceC();
            service.uri = popupLink.parent().parent().firstElementChild().text();
            services.add(service);
            service.tag = createOrGetTag(service);
            service.method = badge.text();

            // Parse the response part
            var response = doc.getElementById(id.substring(1) + "_response");
            assert response != null;
            service.responses = extractResponse(response, types);

            // Parse the request part (if exists)
            var request = doc.getElementById(id.substring(1) + "_request");
            if (request != null) {
                service.request = new RequestC();

                String mode = null;
                for (var ul : request.children()) {
                    // Assumption before the list of properties, an h3 will separate them
                    if (ul.tagName().equals("h3")) {
                        String text = ul.text();
                        if (text.equals("URI Parameters")) {
                            mode = "path";
                        } else if (text.equals("Body")) {
                            mode = "requestBody";
                        }
                    } else if (ul.tagName().equals("ul")) {
                        if (mode == null) {
                            throw new UnsupportedOperationException("Assumption failed: No known h3 came before the list of properties");
                        }
                        if (mode.equals("path")) {
                            for (var li : ul.children()) {
                                PropertyC propertyC = createProperty(li, types);
                                service.request.pathparams.add(propertyC);
                            }
                        } else if (mode.equals("requestBody")) {
                            for (var li : ul.children()) {
                                PropertyC propertyC = createProperty(li, types);service.request.properties.add(propertyC);

                                createChildren(li, propertyC, types);
                            }
                        }
                    }
                }
            }
        }

        types.sort(Comparator.comparing(a -> a.name));
    }

    /**
     * Create a tag (a kind of group for services) if it does not exist yet
     */
    private TagC createOrGetTag(ServiceC service) {
        var tag = new TagC();
        String[] split = service.uri.split("/");
        if (split.length > 2) {
            tag.name = "/" + split[1] + "/" + split[2];
            tag.description = "API for the '" + split[2] + "' " + split[1] + " type";
        } else {
            tag.name = "/" + split[1];
            tag.description = "API for " + split[1] + "s";
        }
        var found = false;
        for (var existing : tags) {
            if (existing.name.equals(tag.name)) {
                tag = existing;
                found = true;
                break;
            }
        }
        if (!found) {
            tags.add(tag);
        }
        return tag;
    }

    private static List<ResponseC> extractResponse(Element response, List<TypeC> types) {
        ResponseC responseC = null;
        List<ResponseC> responses = new ArrayList<>();
        for (var ul : response.children()) {
            if (ul.tagName().equals("h2")) {
                responseC = new ResponseC();
                responses.add(responseC);
                responseC.code = Integer.parseInt(ul.text().substring("HTTP status code ".length()));
            }
            if (ul.tagName().equals("ul")) {
                if (responseC == null) {
                    throw new UnsupportedOperationException("The structure assumes an <h2> always comes before the <ul>");
                }
                for (var li : ul.children()) {
                    PropertyC propertyC = createProperty(li, types);
                    responseC.properties.add(propertyC);

                    createChildren(li, propertyC, types);
                }
            }
        }
        return responses;
    }

    private static void createChildren(Element parent, PropertyC parentC, List<TypeC> types) {
        Element child = parent.selectFirst("ul");
        if (child != null) {
            for (var childProperty : child.select("> li")) {
                if (childProperty.childrenSize() > 0) {
                    PropertyC childC = createProperty(childProperty, types);
                    parentC.type.properties.add(childC);
                    createChildren(childProperty, childC, types);
                }
            }
        }
    }

    private static PropertyC createProperty(Element li, List<TypeC> types) {
        PropertyC propertyC = new PropertyC();
        propertyC.name = li.child(0).text();
        Element child = li.child(1);
        propertyC.required = child.toString().contains("required");
        propertyC.type = new TypeC();
        if (li.childrenSize() > 2) {
            propertyC.description = li.child(2).text();
        }
        {
            String x = child.lastChild().toString();
            propertyC.type.array = x.startsWith("(array of ");
            if (propertyC.type.array) {
                propertyC.type.name = x.substring("(array of ".length(), x.length() - 1);
            } else {
                propertyC.type.name = x.substring(1, x.length() - 1);
            }
            if (propertyC.type.name.contains(" – pattern: ")) {
                String[] split = propertyC.type.name.split(" – pattern: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.pattern = split[1];
            }
            if (propertyC.type.name.contains(" – maximum: ")) {
                String[] split = propertyC.type.name.split(" – maximum: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.maximum = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – minimum: ")) {
                String[] split = propertyC.type.name.split(" – minimum: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.minimum = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – maxLength: ")) {
                String[] split = propertyC.type.name.split(" – maxLength: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.maxLength = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – minLength: ")) {
                String[] split = propertyC.type.name.split(" – minLength: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.minLength = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – maxItems: ")) {
                String[] split = propertyC.type.name.split(" – maxItems: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.maxItems = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – minItems: ")) {
                String[] split = propertyC.type.name.split(" – minItems: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.minItems = Integer.parseInt(split[1]);
            }
            if (propertyC.type.name.contains(" – default: ")) {
                String[] split = propertyC.type.name.split(" – default: ");
                if (split.length != 2) {
                    throw new UnsupportedOperationException(propertyC.type.name);
                }
                propertyC.type.name = split[0];
                propertyC.type.default_ = split[1];
            }
            if (Stream.of("string", "boolean", "number", "integer").anyMatch(s -> propertyC.type.name.equals(s))) {
                propertyC.type.builtin = true;
            }
            if (propertyC.type.name.equals("datetime")) {
                propertyC.type.builtin = true;
                propertyC.type.name = "string";
                propertyC.type.pattern = "\\d[4]-\\d[2]-\\d[2]T\\d[2]:\\d[2]:\\d[2]Z";
            }
            if (propertyC.type.name.equals("time-only")) {
                propertyC.type.builtin = true;
                propertyC.type.name = "string";
                propertyC.type.pattern = "\\d[2]:\\d[2]:\\d[2]";
            }
            if (propertyC.type.name.startsWith("one of ")) {
                propertyC.type.enum_ = true;
                propertyC.type.name = propertyC.type.name.substring("one of ".length());
            }
            if (!propertyC.type.name.equals("object") && !propertyC.type.builtin && !propertyC.type.enum_ && propertyC.type.name.matches("^[a-z_]+$")) {
                propertyC.type.enum_ = true;
            }
        }
        if (!propertyC.type.builtin && !propertyC.type.name.equals("object") && !propertyC.type.enum_) {

            boolean found = false;
            for (var existing : types) {
                if (existing.name.equals(propertyC.type.name)) {
                    // Assume types match ... horribly wrong :-)
                    found = true;
                    break;
                }
            }

            if (!found) {
                types.add(propertyC.type);
            }
        }
        return propertyC;
    }
}
