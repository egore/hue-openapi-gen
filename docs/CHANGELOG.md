## [1.0.1](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/compare/1.0.0...1.0.1) (2024-12-04)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.18.2 ([d76b84b](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/d76b84b4ca61ba5a0d3e921db165d388db08a437))
* **deps:** update dependency org.jsoup:jsoup to v1.18.3 ([2ffeaa7](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/2ffeaa7990a6488f2f70b9cb078abdf9dabff0d3))

# 1.0.0 (2024-10-29)


### Bug Fixes

* **ci:** Add dummy distributionManagement ([601391b](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/601391b523aabb9a60a0586a0ff330868ce89434))
* **deps:** update dependency info.picocli:picocli to v4.7.6 ([81f8020](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/81f80200ce12d1aec2396550d6f9befc448e1053))
* **deps:** update dependency org.jsoup:jsoup to v1.18.1 ([41594d0](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/41594d069acdffc3dbadd5cdc8b9d3a9fd1e083c))


### Features

* Add support for rendering property descriptions ([79dec23](https://gitlab.com/appframework/egore-personal/hue-openapi-gen/commit/79dec238ce47364712263797d779470e11d57ea0))
