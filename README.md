# OpenAPI generator for Hue CLIP API

The CLIP API seems to be written in RAML and is exported using [RAML2HTML](https://github.com/raml2html/raml2html),
which generates parsable HTML. This HTML is parsed and (with lots of assumptions about the structure) parsed into
an OpenAPI spec.

## Creating the openapi.yaml

1. Download the raw HTML of https://developers.meethue.com/develop/hue-api-v2/api-reference/
2. Place it as input.html into the folder of this tool
3. Run the tool

## Using openapi.yaml

1. Grab the file `openapi.yaml`
2. Paste the content into https://editor.swagger.io/
3. Browse it or create generate a client with it